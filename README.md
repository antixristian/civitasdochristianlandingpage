<h1>Civitas</h1>
> Status: Developing ⚒️

### Civitas is a platform for selling products or services. The idea was proposed by EJCM to the trainees of the 2022.1 selection process

How to run:

1) Run: git clone https://gitlab.com/antixristian/civitasdochristianlandingpage.git
2) Open the cloned directory in your IDE
3) Run: npm install
4) Run: npx json-server --watch db.json
5) Enjoy :)