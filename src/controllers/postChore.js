const criarRegistro = async (nome, data, email, senha) => {
    try {
      const response = await fetch("http://localhost:3000/chores", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          nome: `${nome}`,
          data: `${data}`,
          email: `${email}`,
          senha: `${senha}`,
        }),
      });
      const content = await response.json();
      return content;
    } catch (error) {
      console.log(error);
    }
  };
  
  export default criarRegistro;