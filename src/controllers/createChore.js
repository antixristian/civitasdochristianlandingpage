import criarRegistro from "./postChore.js";

const inputNome = document.querySelector("#listNome");
const inputData = document.querySelector("#listData");
const inputEmail = document.querySelector("#listEmail");
const inputSenha = document.querySelector("#listSenha");
const createBtn = document.querySelector("#listBtn");
const errSpan = document.querySelector(".error");

createBtn.addEventListener('click', async event => {
  event.preventDefault();
  const nome = inputNome.value;
  const data = inputData.value;
  const email = inputEmail.value;
  const senha = inputSenha.value;

  if (nome === "" || data === "" || email === "" || senha === "") {
    errSpan.classList.remove("invisible");
  } else {
    await criarRegistro(nome, data, email, senha);
  }
});